use std::collections::HashMap;
use serde::{Deserialize, Serialize};
use serde_php::from_bytes;

#[derive(Debug, PartialEq)]
/**
 * The call pair will be extracted from each entry.
 */
struct CallPair<'a> {
    parent: Option<&'a str>,
    child: &'a str,
}

impl CallPair<'_> {
    /**
     * Takes a parent/child function name encoded as
     * "a==>b" and returns a CallPair.
     */
    fn parse(input: &str) -> Result<CallPair, String> {
        let parts: Vec<&str> = input.split("==>").collect();
        match parts.as_slice() {
            [single] => Ok(CallPair { parent: None, child: single }),
            [parent, child] => Ok( CallPair { parent: Some(parent), child }),
            _ => Err("Invalid input string".to_string()),
        }
    }

    pub fn display(self) -> String {
        self.child.to_string()
    }
}

/**
 * This struct matches what we get when parsing serialized PHP.
 */
#[derive(Debug, Serialize, Deserialize, Eq, PartialEq, Clone)]
struct RawProfile {
    ct: i64,
    wt: i64,
    cpu: i64,
    mu: i64,
    pmu: i64,
}

/**
 * The list of possible metrics collected as part of XHProf that require inclusive/exclusive
 * handling while reporting.
 */
struct MetricSet {
    ct: i64,
    wt: i64,
    excl_wt: i64,
    ut: i64,
    excl_ut: i64,
    st: i64,
    excl_st: i64,
    cpu: i64,
    excl_cpu: i64,
    mu: i64,
    excl_mu: i64,
    pmu: i64,
    excl_pmu: i64,
    samples: u64
}

impl MetricSet {
    /**
     * Read a single metric set.
     */
    fn extract(p: &RawProfile) -> MetricSet {
        MetricSet {
            ct: p.ct,
            wt: p.wt,
            excl_wt: p.wt,
            ut: 0,
            excl_ut: 0,
            st: 0,
            excl_st: 0,
            cpu: p.cpu,
            excl_cpu: p.cpu,
            mu: p.mu,
            excl_mu: p.mu,
            pmu: p.pmu,
            excl_pmu: p.pmu,
            samples: 0
           }
    }
}

/**
 * An XHProf entry comsists of a parent==>child entry with the metric set collected.
 */
struct XHProfEntry<'a> {
    pub id: CallPair<'a>,
    metric: MetricSet,
}

impl XHProfEntry<'_> {
    /**
     * Populate a single XHProf entry
     */
    fn extract<'a>( label: &'a String, profile: &RawProfile ) -> Result<XHProfEntry<'a>, String> {
        match CallPair::parse(label.as_str()) {
            Ok(v) => Ok(XHProfEntry { id: v, metric: MetricSet::extract(profile) }),
            Err(e) => Err(e)
        }
    }
}

/**
 * A representation of the whole XHProf file (without being tied to file semantics, so we could
 * store them in, say, a db).
 */
pub struct XHProfData<'a> {
    pub name: String,
    pub data: std::vec::IntoIter<XHProfEntry<'a>>
}

impl XHProfData<'_> {
    pub fn read( name: String, data: &str ) -> Self {
        let parsed: HashMap<String, RawProfile> = from_bytes(data.as_bytes())
            .expect("deserialization of {name} failed!");

        Self {
            name,
            data: parsed.iter().map(
                |(k, v)|
                match XHProfEntry::extract(k, v) {
                    Ok(v) => v,
                    Err(e) => panic!("{}", e)
                }
            ).collect::<Vec<_>>().into_iter()
        }
    }
}

#[cfg(test)]
mod deserialization_checks {
    use crate::{process, ProcessedProfile, Profile};
    use serde_php::{from_bytes, to_vec};
    use std::collections::HashMap;
    use std::str;

    #[test]
    fn profile() {
        let check =
            "a:5:{s:2:\"ct\";i:1;s:2:\"wt\";i:2;s:3:\"cpu\";i:3;s:2:\"mu\";i:888;s:3:\"pmu\";i:0;}";
        let expect: Profile = Profile {
            ct: 1,
            wt: 2,
            cpu: 3,
            mu: 888,
            pmu: 0,
        };
        let result: Profile = from_bytes(check.as_bytes()).expect("deserialzation failed");

        assert_eq!(expect, result);
    }

    #[test]
    fn simplest_serialize() {
        let prof: Profile = Profile {
            ct: 1,
            wt: 61,
            cpu: 9,
            mu: 856,
            pmu: 0,
        };
        let mut expect = HashMap::new();
        expect.insert("main()".to_owned(), prof);
        let check = "a:1:{s:6:\"main()\";a:5:{s:2:\"ct\";i:1;s:2:\"wt\";i:61;s:3:\"cpu\";i:9;s:2:\"mu\";i:856;s:3:\"pmu\";i:0;}}";
        let result: HashMap<String, Profile> =
            from_bytes(check.as_bytes()).expect("deserialzation failed");

        assert_eq!(expect, result);
    }

    #[test]
    fn simplest_deserialize() {
        let prof: Profile = Profile {
            ct: 1,
            wt: 61,
            cpu: 9,
            mu: 856,
            pmu: 0,
        };
        let mut expect = HashMap::new();
        expect.insert("main()".to_owned(), prof);
        let check = "a:1:{s:6:\"main()\";a:5:{s:2:\"ct\";i:1;s:2:\"wt\";i:61;s:3:\"cpu\";i:9;s:2:\"mu\";i:856;s:3:\"pmu\";i:0;}}";

        let result = to_vec(&expect).expect("serializatoin failed");
        let serialized = match str::from_utf8(&result) {
            Ok(v) => v,
            Err(e) => panic!("Invalid UTF-8 sequence: {e}"),
        };

        assert_eq!(check, serialized);
    }

    // Need indexmap here or something

    // #[test]
    // fn test_more_serialize() {
    //     let check = "a:2:{s:14:\"main()==>hello\";a:5:{s:2:\"ct\";i:1;s:2:\"wt\";i:74;s:3:\"cpu\";i:11;s:2:\"mu\";i:864;s:3:\"pmu\";i:0;}s:6:\"main()\";a:5:{s:2:\"ct\";i:1;s:2:\"wt\";i:81;s:3:\"cpu\";i:15;s:2:\"mu\";i:1432;s:3:\"pmu\";i:0;}}";
    //     let tuples: HashMap<String, Profile> = [
    //         (
    //             "main()==>hello".to_owned(),
    //             Profile {
    //                 ct: 1,
    //                 wt: 74,
    //                 cpu: 11,
    //                 mu: 864,
    //                 pmu: 0,
    //             },
    //         ),
    //         (
    //             "main()".to_owned(),
    //             Profile {
    //                 ct: 1,
    //                 wt: 81,
    //                 cpu: 15,
    //                 mu: 1432,
    //                 pmu: 0,
    //             },
    //         ),
    //     ]
    //     .into_iter()
    //     .collect();
    //     let binding = to_vec(&tuples).unwrap();
    //     let serialized = match str::from_utf8(&binding) {
    //         Ok(v) => v,
    //         Err(e) => panic!("Invalid UTF-8 sequence: {}", e),
    //     };

    //     assert_eq!(serialized, check);
    // }

    #[test]
    fn test_more_deserialize() {
        let check = "a:2:{s:14:\"main()==>hello\";a:5:{s:2:\"ct\";i:1;s:2:\"wt\";i:74;s:3:\"cpu\";i:11;s:2:\"mu\";i:864;s:3:\"pmu\";i:0;}s:6:\"main()\";a:5:{s:2:\"ct\";i:1;s:2:\"wt\";i:81;s:3:\"cpu\";i:15;s:2:\"mu\";i:1432;s:3:\"pmu\";i:0;}}";
        let main_profile = Profile {
            ct: 1,
            wt: 81,
            cpu: 15,
            mu: 1432,
            pmu: 0,
        };
        let tuples: HashMap<String, Profile> = [
            (
                "main()==>hello".to_owned(),
                Profile {
                    ct: 1,
                    wt: 74,
                    cpu: 11,
                    mu: 864,
                    pmu: 0,
                },
            ),
            ("main()".to_owned(), main_profile),
        ]
        .into_iter()
        .collect();
        let result: HashMap<String, Profile> =
            from_bytes(check.as_bytes()).expect("deserialzation failed");

        assert_eq!(tuples, result);
    }

    #[test]
    fn test_process() {
        let input = Profile {
            ct: 1,
            wt: 74,
            cpu: 11,
            mu: 864,
            pmu: 9,
        };
        let key = "main()==>hello".to_string();
        let check = ProcessedProfile {
            callee: "hello".to_string(),
            caller: "main()".to_string(),
            ct: 1,
            wt: 74,
            excl_wt: 0,
            cpu: 11,
            excl_cpu: 0,
            mu: 864,
            excl_mu: 0,
            pmu: 9,
            excl_pmu: 0,
        };

        let mut t = HashMap::new();
        assert_eq!((key.clone(), check), process(key, &input, &mut t));
    }
}

#[cfg(test)]
mod xhprof_parse_test {
    use crate::{CallPair, xhprof_parse_parent_child};
    use rstest::rstest;

    #[rstest]
    #[case( "main()", None, "main()" )]
    #[case( "arbitrary()", None, "arbitrary()" )]
    #[case( "main()==>arbitrary()", Some("main()"), "arbitrary()" )]
    fn main( #[case] input: &str, #[case] parent: Option<&str>, #[case] child: &str) {
        let expect = CallPair { parent, child };
        let result = xhprof_parse_parent_child(input);

        assert_eq!(expect, result);
    }

    #[test]
    #[should_panic]
    fn test_panic() {
        let expect = CallPair { parent: Some("make()"), child: "me()" };
        let result = xhprof_parse_parent_child("make()=>me()");

        assert_eq!(expect, result);
    }

}
