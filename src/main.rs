extern crate lazy_static;

mod xhprof;

use regex::Regex;
use xhprof::XHProfData;
use flate2::read::GzDecoder;
use std::env;
use std::fs::File;
use std::io::Read;
use walkdir::{DirEntry, WalkDir};

fn should_keep(check: &str) -> bool {
    let mut contents = String::new();
    let mut file = File::open("keep-functions.txt")
        .expect("Couldn't open file keep-functions.txt!");
    file.read_to_string(&mut contents).expect("Couldn't read file");
    let lines: Vec<String> = contents.split("\n").map(
        |s| s.replace("\\", "\\\\")).collect();
    let ref re: Regex =
            Regex::new(&format!("\\b(?:{})\\b", lines.join("|")))
                .expect("Couldn't create regular expression");

    if re.is_match(check) {
        return true;
    }
    false
}

fn handle_entry(file: DirEntry) {
    if !file.file_type().is_file() {
        return;
    }
    let path = file.clone().into_path();
    let full_path = match path.to_str() {
        Some(v) => v,
        None => panic!("Couldn't get file path"),
    };
    eprintln!(" Examining {full_path} ...");

    let file_handle = match File::open(full_path) {
        Ok(f) => f,
        Err(e) => panic!("Trouble opening file: {full_path}: {e}"),
    };

    let mut f = GzDecoder::new(file_handle);

    // read into a String, so that you don't need to do the conversion.
    let mut buffer = String::new();

    f.read_to_string(&mut buffer)
        .expect("Trouble Reading file!");

    // Now, let xhprof do the rest.
    let xhprof = XHProfData::read( full_path.to_string(), buffer.as_str() );

    for entry in xhprof.data {
        println!("\t{}", entry.id.display());
    };
}

fn is_xhprof_file(entry: &DirEntry) -> bool {
    let oname =  entry.file_name();
    let name = oname.to_str().expect("Not UTF8");
    let file_type = entry.file_type();
    let len = match entry.metadata() {
        Ok(x) => x.len(),
        Err(e) => panic!("Couldn't get metadata: {e}")
    };
    if len == 0 &&
        file_type.is_file() &&
        (name.ends_with(".xhprof.gz") ||
         name.ends_with(".xhprof")) {
        return true;
    }
    false
}

fn main() {
    let dir = env::var("PAGE_REPO")
        .unwrap_or_else(
            |e|  panic!("Problem while looking for PAGE_REPO: {e}")
        );
    eprintln!("Recursing through {dir}");
    WalkDir::new(dir)
        .into_iter()
        .filter_entry(is_xhprof_file)
        .for_each(|file| match file {
            Ok(entry) => handle_entry(entry),
            Err(e) => eprintln!("Error in processing: {e}"),
        });
}
